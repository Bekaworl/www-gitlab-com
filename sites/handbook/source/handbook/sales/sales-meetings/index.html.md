---
layout: handbook-page-toc
title: "Field Team Meetings"
description: "Overview of the primary meetings for the global GitLab Field team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The below page overviews the primary meetings for the WW Field Team. 

## Field All-Hands Meetings
The team meets as a full group multiple times in a given quarter, with each meeting series serving a different enablement or planning purpose. 

The high-level cadence of Field full-team meetings in a given quarter is: 

1. **Month 1**
   1. [WW Field Sales Call](/handbook/sales/sales-meetings/#ww-field-sales-call)
   1. [Quarterly Business Reviews (QBRs)](/handbook/sales/sales-meetings/#quarterly-business-reviews)
   1. **[In Pilot]** [CRO Segment Roundtable](/handbook/sales/sales-meetings/#fy24-pilot-cro-segment-roundtable)
1. **Month 2**
   1. [Sales, Marketing, Product QBR & Recognition (QBR&R)](/handbook/sales/sales-meetings/#sales-marketing-product-qbr--recognition-qbrr-call)
   1. **[In Pilot]** [All Field Manager Call](/handbook/sales/sales-meetings/#fy24-pilot-all-field-manager-call)
1. **Month 3**
   1. [WW Field Sales Call](/handbook/sales/sales-meetings/#ww-field-sales-call)

### WW Field Sales Call

* **When:** Every first and third month of the quarter from 9:00am to 9:50am Pacific time. Happens within the first seven business days of a month.
   * In the second month of the quarter, we host the [Sales, Marketing, Product QBR & Recognition call](/handbook/sales/sales-meetings/#sales-marketing-product-qbr--recognition-qbrr-call), which has an expanded audience and a modified agenda.
   * The WW Field Sales Call is rescheduled to avoid conflict with major public holidays and/or executive schedules (ex. E-Group offsite).
* **Who:** This meeting includes all members of the CRO organization as well as stakeholders of the Field team from the Marketing, Finance and People Groups.
* **What:** The primary purpose of the call is to drive knowledge-sharing across the Field organization, with an emphasis on CRO and leadership business/organizational updates and Q&A time for Field team members and leadership. The typical format is: 
   1. CRO Organizational Update 
      1. High-priority topics/announcements for the CRO and CRO LT 
      1. Recognition for all Field departments 
      1. Field Team Momentum 
   1. Guest Speaker Updates 
   1. Field Team AMA 
   1. Update Ticker (included asynchronously)
* If you would like to add topics to the agenda, please coordinate with Field Communications using their [announcement request process.](/handbook/sales/field-communications/#requesting-field-announcements).

#### General Guidelines for Attendees

1. This call should be a discussion/review rather than a presentation. Please ask questions – this is our chance to learn together as a team. Add questions to the Q&A section of the agenda.
1. Any updates that are not covered on the call will be addressed asynchronously in the agenda. Please read the agenda in full each time.
1. The agenda can be found [here](https://docs.google.com/document/d/1CVSz5hU5XoFbVtZoBxXsQcqD7AII0NW2KBzy3efA3PI/edit).
1. Zoom recording links for past calls are included inline in the agenda document under the meeting date. 
1. If you cannot join the call, consider reviewing the recorded call, or at minimum, read through the meeting agenda.
1. If you have feedback on how we can improve the WW Field Sales Call, [share it with Field Communications.](/handbook/sales/field-communications/#sharing-feedback)

#### General Guidelines for Presenters

1. Provide your final agenda times by end of day on the business day before the meeting. Your agenda updates should link to relevant references (Handbook pages, issues, MRs, etc.) that provide more context, and you should include where the field can go with more questions and/or feedback that isn't discussed on the call.
1. Please be respectful of time and keep your updates to 1-2 minutes max – focusing on the most impactful learning or takeaway. We want to leave ample time for questions.
   1. If you feel your update requires more context, consider recording a quick video with more background that the field can watch asynchronously.
1. Slides are not necessary, but if you would like to present slides, ensure that you also include a link to the presentation in the agenda.
1. Once you are done presenting, please handoff to the next presenter on the agenda.

### Sales, Marketing, Product QBR & Recognition (QBR&R) Call

* **When:** (Previously called the GTM Field Update.) The QBR&R call takes place after each GitLab quarterly earnings call. 
* **Who:** The teams included on the invite for this call are: 
   1. Field division
   1. Marketing division
   1. Product division 
   1. Field stakeholders (PBPs, Recruiters, Finance)
* **What:** The goal of this call is to:
   1. Recognize and thank Field team members for their contributions to GitLab's success
   1. Celebrate the wins and learn from the misses as a team
   1. Drive transparency and accountability across Sales, Marketing and Product, with all leaders sharing:
      1. A review of the deliverables promised in the previous quarter and the status of achievement toward those deliverables    
      1. The top 3 commitments from each division in the current quarter 
* If you would like to add topics to the agenda, please coordinate with Field Communications using their [announcement request process.](/handbook/sales/field-communications/#requesting-field-announcements).

### Quarterly Business Reviews

For a full overview of the QBR planning and execution process, QBR request tracking, best-practices for organizers and attendees, and where to find past QBR content, [see here](/handbook/sales/qbrs). 

### [FY24 Pilot] CRO Segment Roundtable

This call was launched in Q2-FY24 and is being run by the Field team as a pilot throughout the fiscal year.

* **When:** This meeting happens quarterly. The meeting will last for 50 min. 
* **Who:** Field segment (ENT, COMM, CS, Partner Team, Field Operations) participation will rotate quarterly, so each segment will have one roundtable with the CRO in a fiscal year. (Note: One quarter must feature two segments.) The organization/planning of the roundtable, including timing and attendance criteria, is at the discretion and responsibility of the CRO leader for a given segment. The CRO leader will work directly with the CRO 
* **What:** The purpose of these meetings is to give Field team members face-time and build connection with the CRO. The meeting is unstructured, and should feel like a "group coffee chat" with the CRO unless otherwise outlined by the segment leader. The CRO and CRO leader can choose to focus on a specific topic and have prepared remarks, simply use the entire time for an AMA with a specific team within that segment, etc.

### [FY24 Pilot] All Field Manager Call

This call was launched in Q2-FY24 and is being run by the Field team as a pilot throughout the fiscal year.

* **When:** This meeting happens quarterly in the second month of each quarter after GitLab's earnings call. The call is 50 minutes.
* **Who:** All Field Managers (people managers and above in the CRO's organization), People Business Partners supporting the Field, Sales Development Management
* **What:** This meeting operates like a mini-Group Conversation and places an emphasis on driving awareness for groups across the Field org, highlighting/recognizing front-line managers who are excelling in performance and team development/coaching, and socializing key priorities and CTAs for managers. Team members from Field Enablement and Field Communications will partner with CRO leaders to organize the call agenda and speakers each quarter. The goal of this meeting is to drive increased manager engagement and enablement by:
   1. Providing Field people leaders time directly with the CRO and hearing their perspective on:
      1. Where we're at
      1. Where we're going
      1. What managers have to look forward to
      1. CTAs/how managers will help us get there
   1. Providing exposure to other members of CRO LT and fostering a united front across the Field team.
   1. Creating awareness around key initiatives/projects launching in the upcoming quarter that will have a direct impact on Field teams


## Enablement/Planning/Strategy Meetings

### Weekly Thursday Field Enablement Webcast led by the Field Enablement team
* **When:** Weekly on Thursday at 9am Pacific time, except in the last two weeks of a quarter
* **Who:** The entire GitLab Field team is invited to this meeting series. The series can be found on the [Field Enablement Google Calendar](https://calendar.google.com/calendar/u/0?cid=Z2l0bGFiLmNvbV81bjNnNjBsNTh0aHVtOWFvdnA4aWlzYXYzNEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t).
* **What:** The purpose of this series is to enable Field team members on the latest resources and selling tactics. Enablement topics vary weekly. See more on this [handbook page](/handbook/sales/training/sales-enablement-sessions/).

### Sales Group Conversation
* **When:** Happens once every 8 weeks at 8am Pacific time
* **Where:** Zoom link varies - please be sure to check the calendar invitation
* **What:** The CRO Leaders will rotate and provide the company with an update on their business, overall team performance, notable wins, accomplishments, and relevant strategies planned.
You can view previous updates in the "Group Conversation Sales" (previously "Functional Group Update Sales") folder on the Google Drive.
   * CRO Rotation: Michael McBride (CRO), Commercial Sales, Customer Success, Enterprise Sales, Channels

### Sales Monthly [Key Review](/handbook/key-review/)
* **When:** Happens once every month
* **Where:** Zoom link varies - please check the calendar invitation
* **What:** Michael McBride (CRO) will present to the CEO, CFO, and other company leaders a monthly Sales update. 

This update will include the current status of:
   * Sales KPI's
   * [OKR's](https://about.gitlab.com/company/okrs/fy21-q3/)

The meeting will cover:
   * the quarterly forecast
   * QTD performance
   * most recent month's performance

We will provide key takeaways as to what is on-track (green), needs improvement (yellow), or is of urgent concern (red). 

We will review the standard IACV/ARR look back on historical performance, as well as the Pipeline X-Ray, which includes future-looking metrics like pipeline generation, coverage, and pacing for the current and next quarters.

### Focus Fridays

The Field team practices Focus Fridays. The goal is to encourage all field team members to clear out their calendars on Fridays as much as possible in hopes that this will help all of us be more efficient, allow space within our weeks for focused work, and reduce potential burnout. Guidance for Focus Fridays includes:  

- Cancel or move any standing meetings occurring on Fridays
- Customer/prospect/partner/external meetings are exceptions
- Some one-off internal meetings might not be avoidable, but aim to minimize them as much as possible
- Consider blocking off your calendar as "busy" on Fridays to block your work time
- Consider looking into apps like [Clockwise](https://www.getclockwise.com/) that can provide recommendations on how to refactor your calendar for focused work 

You are encouraged to talk to your manager for guidance on how best to embrace Focus Fridays on your team and with your individual schedule. 
