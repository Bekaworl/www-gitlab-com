---
layout: handbook-page-toc
title: "FedRAMP Vulnerability Scanning and Triage Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## FedRAMP Vulnerability Scanning and Triage Process

This content has moved to the internal handbook: <https://internal-handbook.gitlab.io/handbook/security/application_security/runbooks/fedramp-scanner-triage-process/>.